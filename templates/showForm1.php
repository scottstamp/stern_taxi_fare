<?php
function showForm1($atts)
{

    $args = array(
        'post_type' => 'stern_taxi_car_type',
        'nopaging' => true,
        'order' => 'ASC',
        'orderby' => 'meta_value',
        'meta_key' => '_stern_taxi_car_type_organizedBy'
    );
    $allTypeCars = get_posts($args);

    $args = array(
        'post_type' => 'stern_listAddress',
        'nopaging' => true,
        'meta_query' => array(
            array(
                'key' => 'typeListAddress',
                'value' => 'source',
                'compare' => '=',
            ),
        ),
        'meta_query' => array(
            array(
                'key' => 'isActive',
                'value' => 'true',
                'compare' => '=',
            ),
        ),
    );
    $allListAddressesSource = get_posts($args);

    $args = array(
        'post_type' => 'stern_listAddress',
        'nopaging' => true,
        'meta_query' => array(
            array(
                'key' => 'typeListAddress',
                'value' => 'destination',
                'compare' => '=',
            ),
            'meta_query' => array(
                array(
                    'key' => 'isActive',
                    'value' => 'true',
                    'compare' => '=',
                ),
            ),
        ),
    );
    $allListAddressesDestination = get_posts($args);


    $backgroundColor = get_option('stern_taxi_fare_bcolor');
    if ($backgroundColor != "") {
        $backgroundColor = 'background-color:' . stern_taxi_fare_hex2rgba($backgroundColor, 0.7);
    }
    global $woocommerce;
    $checkout_url = $woocommerce->cart->get_checkout_url();
    $currency_symbol = get_woocommerce_currency_symbol();

    if (isBootstrapSelectEnabale()) {
        $class = "form-control";
    } else {
        $class = "selectpicker show-tick";
    }

    $stern_taxi_fare_book_button_text = get_option('stern_taxi_fare_book_button_text');


    $class_full_row6 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-6 col-md-6 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";
    $class_full_row3 = (get_option('stern_taxi_fare_full_row') != "true") ? "class='col-lg-3 col-md-3 col-sm-12 col-xs-12'" : "class='col-lg-12 col-md-12 col-sm-12 col-xs-12'";


    ?>


    <div class="container">
        <div class="stern-taxi-fare">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-lg-6" id="main1"
                     style="<?php echo $backgroundColor; ?>;padding-bottom: 5px">


                    <form id="stern_taxi_fare_div" method="post" action="<?php echo $checkout_url; ?>">


                        <div class="row">
                            <?php
                            if (getShow_dropdown_typecar() == 'false') {
                                $visibility = "visibility: hidden;";
                            } else {
                                $visibility = "";
                            }
                            ?>

                            <div id="typeCarsDropDown" <?php echo $class_full_row6; ?>
                                 style="padding-top: 15px;<?php echo $visibility ?>">


                                <?php if (showlabel()) : ?>
                                    <label for="cartypes"><?php _e('Type', 'stern_taxi_fare'); ?></label>
                                <?php endif; ?>


                                <select name="cartypes" id="cartypes" class="<?php echo $class; ?>" data-width="100%"
                                        style="padding-left: 15px; float: right;">';
                                    <optgroup label="Type">
                                        <?php foreach ($allTypeCars as $post) : setup_postdata($post); ?>
                                            <?php $postId[] = $post->ID; ?>
                                            <option data-icon="glyphicon-road" value="<?php echo $post->ID; ?>">
                                                <?php echo get_post_meta($post->ID, '_stern_taxi_car_type_cartype', true); ?>
                                            </option>

                                        <?php endforeach; ?>
                                    </optgroup>
                                </select>

                            </div>

                            <?php $visibility = (get_option('stern_taxi_fare_round_trip') == 'false') ? "visibility: hidden;" : ""; ?>


                            <div <?php echo $class_full_row3; ?> style="padding-top: 15px;<?php echo $visibility ?>">
                                <?php if (showlabel()) : ?>
                                    <label
                                        for="stern_taxi_fare_round_trip"><?php _e('Round Trip', 'stern_taxi_fare'); ?></label>
                                <?php endif; ?>
                                <select name="stern_taxi_fare_round_trip" id="stern_taxi_fare_round_trip"
                                        class="<?php echo $class; ?>" data-width="100%"
                                        style="padding-left: 15px; float: right;">
                                    <optgroup id="roundTrip" label="<?php _e('Round Trip', 'stern_taxi_fare'); ?>">
                                        <option data-icon='glyphicon-arrow-right'
                                                value="false"> <?php _e('One Way', 'stern_taxi_fare'); ?></option>
                                        <option data-icon='glyphicon-random'
                                                value="true"> <?php _e('Return', 'stern_taxi_fare'); ?></option>
                                    </optgroup>
                                </select>
                            </div>

                            <div <?php echo $class_full_row3; ?> style="padding-top: 15px;">


                                <?php if (showlabel()) : ?>
                                    <label for="baby_count"><?php _e('Seats', 'stern_taxi_fare'); ?></label>
                                <?php endif; ?>


                                <select name="baby_count" id="baby_count" class="<?php echo $class; ?>"
                                        data-width="100%" style="padding-left: 15px; float: right;">
                                    <optgroup id="labelSeats" label="<?php _e('Seats', 'stern_taxi_fare'); ?>">
                                        <?php

                                        $stern_taxi_fare_max_car_seats = get_post_meta($postId[0], '_stern_taxi_car_type_carseat', true);

                                        for ($i = 0; $i <= $stern_taxi_fare_max_car_seats; $i++) {
                                            if ($i == 1) $selected = "selected";
                                            else $selected = "";
                                            echo "<option data-icon='glyphicon-user' value='$i' $selected>$i</option>";
                                        }

                                        ?>

                                    </optgroup>
                                </select>
                            </div>

                            <div <?php echo $class_full_row3; ?> style="padding-top: 15px;">


                                <?php if (showlabel()) : ?>
                                    <label for="child_count"><?php _e('Children', 'stern_taxi_fare'); ?></label>
                                <?php endif; ?>


                                <select name="child_count" id="child_count" class="<?php echo $class; ?>"
                                        data-width="100%" style="padding-left: 15px; float: right;">
                                    <optgroup id="labelChildSeats" label="<?php _e('Children', 'stern_taxi_fare'); ?>">
                                        <?php

                                        $stern_taxi_fare_max_child_seats = get_post_meta($postId[0], '_stern_taxi_car_type_childseat', true);

                                        for ($i = 0; $i <= $stern_taxi_fare_max_child_seats; $i++) {
                                            if ($i == 0) $selected = "selected";
                                            else $selected = "";
                                            echo "<option data-icon='glyphicon-user' value='$i' $selected>$i</option>";
                                        }

                                        ?>

                                    </optgroup>
                                </select>
                            </div>

                            <div <?php echo $class_full_row3; ?> style="padding-top: 15px;">


                                <?php if (showlabel()) : ?>
                                    <label for="infants_count"><?php _e('Infants', 'stern_taxi_fare'); ?></label>
                                <?php endif; ?>


                                <select name="infants_count" id="infants_count" class="<?php echo $class; ?>"
                                        data-width="100%" style="padding-left: 15px; float: right;">
                                    <optgroup id="labelInfantSeats" label="<?php _e('Infants', 'stern_taxi_fare'); ?>">
                                        <?php
                                        $stern_taxi_fare_max_infant_seats = get_post_meta($postId[0], '_stern_taxi_car_type_infantseat', true);

                                        for ($i = 0; $i <= $stern_taxi_fare_max_infant_seats; $i++) {
                                            if ($i == 0) $selected = "selected";
                                            else $selected = "";
                                            echo "<option data-icon='glyphicon-user' value='$i' $selected>$i</option>";
                                        }

                                        ?>

                                    </optgroup>
                                </select>
                            </div>


                        </div>


                        <input style="display: none;" type="text" hidden class="form-control" id="stops_count_s"
                               name="stops_count">
                        <input style="padding-left: 15px; width: 75%; float: right;" class="form-control" type="hidden"
                               value="0" min="0" name="stops_count" id="stops_count">

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 15px;">
                                <div class="input-group form-group" id="divSource">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary " id="cal4" name="submit"
                                                value="getLocation" onClick="getLocation()"
                                                style="font-size: 14px; font-weight: bold">
                                            <span id="getLocationSource" class="glyphicon glyphicon-map-marker"
                                                  aria-hidden="true"></span>
                                        </button>
                                    </div>
                                    <?php if (get_option('stern_taxi_use_list_address_source') == 'true') : ?>
                                        <select name="source" id="source" class="<?php echo $class; ?>"
                                                data-width="100%" style="padding-left: 15px; float: right;">';
                                            <optgroup label="<?php _e('Source', 'stern_taxi_fare'); ?>">
                                                <?php foreach ($allListAddressesSource as $post) : setup_postdata($post); ?>
                                                    <?php $oListAddress = new listAddress($post->ID); ?>
                                                    <option data-icon="glyphicon-import"
                                                            value="<?php echo $oListAddress->getaddress(); ?>">
                                                        <?php echo $oListAddress->getaddress(); ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        </select>
                                    <?php else: ?>
                                        <input id="source" name="source" type="text" class="form-control"
                                               placeholder="<?php _e('Source address', 'stern_taxi_fare'); ?>">
                                    <?php endif ?>
                                </div>

                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 15px;">
                                <div class="input-group form-group" id="divDestination">
                                    <div class="input-group-btn">
                                        <button type="button" class="btn btn-primary " id="cal5" name="submit"
                                                value="showMap" onClick="getLocationDestination()"
                                                style="font-size: 14px; font-weight: bold">
                                            <span id="getLocationDestination"
                                                  class="glyphicon <?php echo getDestination_Button_glyph(); ?>"
                                                  aria-hidden="true"></span>
                                        </button>
                                    </div>
                                    <?php if (get_option('stern_taxi_use_list_address_destination') == 'true') : ?>
                                        <select name="destination" id="destination" class="<?php echo $class; ?>"
                                                data-width="100%" style="padding-left: 15px; float: right;">
                                            <optgroup label="<?php _e('Destination', 'stern_taxi_fare'); ?>">
                                                <?php foreach ($allListAddressesDestination as $post) : setup_postdata($post); ?>
                                                    <?php $oListAddress = new listAddress($post->ID); ?>
                                                    <option data-icon="glyphicon-export"
                                                            value="<?php echo $oListAddress->getaddress(); ?>">
                                                        <?php echo $oListAddress->getaddress(); ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </optgroup>
                                        </select>
                                    <?php else: ?>
                                        <input id="destination" name="destination" type="text" class="form-control"
                                               placeholder="<?php _e('Drop Off Address', 'stern_taxi_fare'); ?>">
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>


                        <input type="hidden" name="stern_taxi_fare_use_calendar" id="stern_taxi_fare_use_calendar"
                               value="<?php echo get_option('stern_taxi_fare_use_calendar'); ?>"/>


                        <div class="row">
                            <div class="col-xs-12 form-group"
                                 style="text-align: center;padding-top: 15px; margin-bottom: 15px">
                                <?php $tooltip = (get_option('stern_taxi_fare_show_tooltips') != "false") ? "data-toggle='tooltip' data-placement='left' data-original-title='" . __('Check Price', 'stern_taxi_fare') . "';" : ""; ?>
                                <button type="button" <?php echo $tooltip; ?> class="btn btn-primary " id="cal1"
                                        name="submit" value="Check" onClick="doCalculation()"
                                        style="font-size: 14px; font-weight: bold"/>
                                <span id="SpanCal1" class="glyphicon glyphicon-check" aria-hidden="true"></span>
                                </button>


                                <?php $tooltip = (get_option('stern_taxi_fare_show_tooltips') != "false") ? "data-toggle='tooltip' data-placement='top' data-original-title='" . __('Show Map', 'stern_taxi_fare') . "';" : ""; ?>
                                <button type="button" <?php echo $tooltip; ?> class="btn btn-primary " id="cal3"
                                        name="submit" value="showMap" onClick="showMap()"
                                        style="font-size: 14px; font-weight: bold;visibility: hidden">
                                    <span id="SpanCal3" class="glyphicon glyphicon-map-marker"
                                          aria-hidden="true"></span>
                                </button>


                                <?php $tooltip = (get_option('stern_taxi_fare_show_tooltips') != "false") ? "data-toggle='tooltip' data-placement='right' data-original-title='" . __('Reset Form', 'stern_taxi_fare') . "';" : ""; ?>
                                <button type="button" <?php echo $tooltip; ?> class="btn btn-primary " id="resetBtn"
                                        name="reset" value="Reset" style="font-size: 14px; font-weight: bold;"/>
                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                </button>

                            </div>

                        </div>


                        <input type="hidden" name="actualVersionPlugin" id="actualVersionPlugin" readonly
                               value="<?php echo getPluginVersion(); ?>"/>
                        <input type="hidden" name="checkout_url" id="checkout_url" readonly
                               value="<?php echo $checkout_url; ?>"/>
                        <input type="hidden" name="currency_symbol" id="currency_symbol" readonly
                               value="<?php echo $currency_symbol; ?>"/>
                        <input type="hidden" name="countryHidden" id="countryHidden"
                               value="<?php echo get_option('stern_taxi_fare_country'); ?>"/>
                        <input type="hidden" name="stern_taxi_fare_url_gif_loader" id="stern_taxi_fare_url_gif_loader"
                               value="<?php echo getUrlGifLoader(); ?>"/>
                        <input type="hidden" name="apiGoogleKey" id="apiGoogleKey"
                               value="<?php echo get_option('stern_taxi_fare_apiGoogleKey'); ?>"/>
                        <input type="hidden" name="stern_taxi_fare_show_map" id="stern_taxi_fare_show_map"
                               value="<?php echo get_option('stern_taxi_fare_show_map'); ?>"/>
                        <input type="hidden" name="stern_taxi_fare_auto_open_map" id="stern_taxi_fare_auto_open_map"
                               value="<?php echo get_option('stern_taxi_fare_auto_open_map'); ?>"/>
                        <input type="hidden" name="getKmOrMiles" id="getKmOrMiles"
                               value="<?php echo getKmOrMiles(); ?>"/>
                        <input type="hidden" name="stern_taxi_fare_avoid_highways_in_calculation"
                               id="stern_taxi_fare_avoid_highways_in_calculation"
                               value="<?php echo get_option('stern_taxi_fare_avoid_highways_in_calculation'); ?>"/>
                        <input type="hidden" name="getShow_use_img_gif_loader" id="getShow_use_img_gif_loader"
                               value="<?php echo getShow_use_img_gif_loader(); ?>"/>
                        <input type="hidden" name="stern_taxi_fare_address_saved_point"
                               id="stern_taxi_fare_address_saved_point"
                               value="<?php echo get_option('stern_taxi_fare_address_saved_point') ?>"/>
                        <input type="hidden" name="stern_taxi_fare_address_saved_point2"
                               id="stern_taxi_fare_address_saved_point2"
                               value="<?php echo get_option('stern_taxi_fare_address_saved_point2') ?>"/>
                        <input type="hidden" name="First_date_available_in_hours" id="First_date_available_in_hours"
                               value="<?php echo getFirst_date_available_in_hours(); ?>"/>
                        <input type="hidden" name="First_date_available_roundtrip_in_hours"
                               id="First_date_available_roundtrip_in_hours"
                               value="<?php echo getFirst_date_available_roundtrip_in_hours(); ?>"/>
                        <input type="hidden" name="stern_taxi_fare_Time_To_add_after_a_ride"
                               id="stern_taxi_fare_Time_To_add_after_a_ride"
                               value="<?php echo get_option('stern_taxi_fare_Time_To_add_after_a_ride') ?>"/>
                        <input type="hidden" name="stern_taxi_fare_great_text" id="stern_taxi_fare_great_text"
                               value="<?php _e('Great! ', 'stern_taxi_fare'); ?>"/>
                        <input type="hidden" name="stern_taxi_fare_fixed_price_text"
                               id="stern_taxi_fare_fixed_price_text"
                               value="<?php _e('This is a fixed price ! ', 'stern_taxi_fare'); ?>"/>

                        <div id="divAlertError" class="alert alert-danger alert-dismissible" role="alert"
                             style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <div id="divAlertErrorText">
                                <strong><?php _e('Error!', 'stern_taxi_fare'); ?></strong><?php _e('Please Try again', 'stern_taxi_fare'); ?>
                            </div>
                        </div>


                        <div id="divAlert" class="alert alert-success alert-dismissible" role="alert"
                             style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                            <div id="divAlertText"></div>
                        </div>


                        <div class="row">


                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" id="resultRightMain"
                                 style="display: inline-block;">

                                <div id="resultText" style="display: none;<?php echo $backgroundColor; ?>;">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="display: inline-block;">

                                            <?php if (get_option('stern_taxi_fare_show_estimated_in_form') != "false") : ?>
                                                <span>
																<strong><?php _e('Estimated Fare: ', 'stern_taxi_fare'); ?></strong><span
                                                        id="estimatedFareSpanValue"></span>
															</span>
                                            <?php endif; ?>

                                            <?php if (get_option('stern_taxi_fare_show_distance_in_form') != "false") : ?>
                                                <br>
                                                <span>
																<strong><?php _e('Distance: ', 'stern_taxi_fare'); ?></strong><span
                                                        id="distanceSpanValue"></span>
															</span>
                                            <?php endif; ?>

                                            <?php if (get_option('stern_taxi_fare_show_tolls_in_form') != "false") : ?>
                                                <br>
                                                <span>
																<strong><?php _e('Tolls: ', 'stern_taxi_fare'); ?></strong><span
                                                        id="tollSpanValue"></span>
															</span>
                                            <?php endif; ?>
                                        </div>
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6" style="display: inline-block;">
                                            <?php if (get_option('stern_taxi_fare_show_duration_in_form') != "false") : ?>
                                                <span>
																<strong><?php _e('Duration: ', 'stern_taxi_fare'); ?></strong><span
                                                        id="durationSpanValue"></span>
															</span>
                                            <?php endif; ?>

                                            <?php if (get_option('stern_taxi_fare_show_suitcases_in_form') != "false") : ?>
                                                <br>
                                                <span>
																<strong><?php _e('Max suitcases: ', 'stern_taxi_fare'); ?></strong><span
                                                        id="suitcasesSpanValue"></span>
															</span>
                                            <?php endif; ?>
                                        </div>
                                    </div>


                                </div>
                            </div>
                        </div>

                        <div id="resultLeft" style="display: none;">

                            <div class="row">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 15px;">
                                    <?php if (showlabel()) : ?>
                                        <label for="dtp_input1"
                                               class="col-md-3 control-label"><?php _e('DateTime Picking', 'stern_taxi_fare'); ?></label>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepickerDateNotCalendar'>
                                            <?php $tooltip = (get_option('stern_taxi_fare_show_tooltips') != "false") ? "data-toggle='tooltip' data-placement='bottom' data-original-title='" . __('DateTime Picking', 'stern_taxi_fare') . "';" : ""; ?>
                                            <div class="input-group-btn" <?php echo $tooltip; ?>>
                                                <button type="button" id="setNowDateId" onClick="setNowDate()"
                                                        class="btn btn-primary "
                                                        style="font-size: 14px; font-weight: bold">
                                                    <span id="buttonDateTime" class="glyphicon glyphicon-time"
                                                          aria-hidden="true"></span>
                                                </button>
                                            </div>
                                            <input type='text' class="form-control" id="dateTimePickUp"
                                                   name="dateTimePickUp"/>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div class="row" id="divDateTimePickUpRoundTrip" style="display: none;">
                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="padding-top: 15px;">
                                    <?php if (showlabel()) : ?>
                                        <label for="dtp_input1"
                                               class="col-md-3 control-label"><?php _e('DateTime Picking for round trip', 'stern_taxi_fare'); ?></label>
                                    <?php endif; ?>
                                    <div class="form-group">
                                        <div class='input-group date' id='datetimepickerRoundTrip'>
                                            <?php $tooltip = (get_option('stern_taxi_fare_show_tooltips') != "false") ? "data-toggle='tooltip' data-placement='bottom' data-original-title='" . __('DateTime Picking for round trip', 'stern_taxi_fare') . "';" : ""; ?>
                                            <div class="input-group-btn" <?php echo $tooltip; ?>>
                                                <button type="button" id="setNowDateId" onClick="setNowDateRoundTrip()"
                                                        class="btn btn-primary "
                                                        style="font-size: 14px; font-weight: bold">
                                                    <span id="buttonDateTimeROundTrip"
                                                          class="glyphicon glyphicon-dashboard"
                                                          aria-hidden="true"></span>
                                                </button>
                                            </div>
                                            <input type='text' class="form-control" id="dateTimePickUpRoundTrip"
                                                   name="dateTimePickUpRoundTrip"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button type="button" class="btn btn-primary " onclick="checkout_url_function()"
                                    id="calCheckout_url" name="calCheckout_url" value="calCheckout_url"
                                    style="font-size: 14px; font-weight: bold"/>
                            <span id="SpanBookButton" class="glyphicon glyphicon-ok"
                                  aria-hidden="true"> </span> <?php echo $stern_taxi_fare_book_button_text; ?>
                            </button>

                        </div>


                    </form>
                </div>

                <div class="col-xs-12 col-sm-6 col-lg-6" id="main2"
                     style="<?php echo $backgroundColor; ?>; background-image:url(<?php echo getUrlGifLoader(); ?>);">


                </div>


            </div>
        </div>


    </div>
    <?php
    /*
echo "<pre>";
    var_dump(WC()->session);
echo "</pre>";
    */

}
