<?php

if ( ! defined( 'ABSPATH' ) )
	exit;


Class templateRule{
	function __construct(){	
		?>
			
			<div class="wrap"><div id="icon-tools" class="icon32"></div>
				<h2>Pricing Rules</h2>
			</div>
			<div class="wrap"><div id="icon-tools" class="icon32"></div>
				<h2>Warning!</h2> Google map APIs is free, but you have a limitaion. We advie to use a maximum 4 rules for the Free maps API. See details here : <a href="https://developers.google.com/maps/documentation/business/faq#usage_limits">Usage limits for the Google Maps API</a>.
			</div>
			
		<?php
			$args = array(
			'post_type' => 'stern_taxi_rule',
			'posts_per_page' => 200,
			);

			$allRules = get_posts( $args );				
		
		?>		
			<br>
			<form method="post">
				<table class="displayrecord">
					<thead  align="left">
						<tr class="home">
							<th>Id</th>
							<th>Is Active</th>
							<th>Name Rule</th>
							<th>Type source</th>
							<th>Type source value</th>
							<th>Type Destination</th>
							<th>Type Destination Value</th>
							<th>Type Car</th>
							<th>Price for this rule</th>							
							<th>Delete</th>
						</tr>
					</thead>
					<tbody>
					<?php
					foreach ( $allRules as $post ) : setup_postdata( $post );
					$oRule = new rule($post->ID);
					?>
					
						<tr>
							<td><?php echo $oRule->getid() ?></td>
							<td>
								<?php 
									if ($oRule->getisActive() =="true") { $checked = "checked";	} else {$checked = "";}
								?>
								<input type="checkbox" name="isActive<?php echo $post->ID; ?>" value="true" <?php echo $checked; ?>>
								
								
							</td>
							<td><?php echo $oRule->getnameRule(); ?></td>
							<td><?php echo $oRule->gettypeSource(); ?></td>
							<td><?php echo $oRule->gettypeSourceValue(); ?></td>
							<td><?php echo $oRule->gettypeDestination(); ?></td>
							<td><?php echo $oRule->gettypeDestinationValue(); ?></td>
							<?php 
								$otypeCar = new typeCar($oRule->gettypeIdCar() ); 
								if($otypeCar->getcarType()=="") { $typeCar = "All";} else { $typeCar = $otypeCar->getcarType();}
							?>
							<td><?php echo $typeCar; ?></td>					
							<td><?php echo $oRule->getprice(); ?></td>							
							<td><input type="checkbox" name="remove<?php echo $post->ID; ?>" value="yes"></td>
						</tr>
					   
					<?php endforeach; ?>
					<?php wp_reset_postdata(); ?>	
						<tr>
							<td></td>
							<td>							
								<select name="isActive" >
									<option value="true" >true</option>
									<option value="false" >false</option>					
								</select>
							</td>
							
							<td><input type="text" name="nameRule"></td>
							
							<td>
								<select name="typeSource" >
									<option value="address" >address</option>
									<option value="city" >city</option>
									<option value="all" >all</option>
								</select>							
							</td>
							<td><input type="text" id="typeSourceValue" name="typeSourceValue"></td>
							<td>
								<select name="typeDestination" >
									<option value="address" >address</option>
									<option value="city" >city</option>									
									<option value="all" >all</option>
								</select>								
							</td>
							<td><input type="text" id="typeDestinationValue" name="typeDestinationValue"></td>
							<?php 
								$args = array(
								'post_type' => 'stern_taxi_car_type',
								'posts_per_page' => 200,
								);

								$allPosts = get_posts( $args );
							?>			
					

							<td>
								<select name="typeIdCar" >
									<option value="" >All</option>
									<?php foreach ( $allPosts as $post ) : setup_postdata( $post ); ?>
									<?php $otypeCar = new typeCar($post->ID); ?>																	
										<option value="<?php echo $otypeCar->getid(); ?>" ><?php echo $otypeCar->getcarType(); ?></option>
									<?php endforeach; ?>
								</select>								
							</td>							
							
							<td><input type="number" step="0.1" name="price"></td>
							<td><input type="submit" id="ruleSubmit" value="Go" class="button-primary" name="ruleSubmit" /></td>
						</tr>
						<input type="hidden"  name="countryHidden" id="countryHidden" value="<?php echo get_option('stern_taxi_fare_country'); ?>"/>
					</tbody>
				</table>
				<br>
				<br/>
				<h4><?php _e('Bulk upload', 'stern_taxi_fare'); ?></h4>
				<?php _e('Use tab to split each colums. 1 rule per row.', 'stern_taxi_fare'); ?>
				<br>
				<?php _e('Example:', 'stern_taxi_fare'); ?>
				<br>
				<input type="text" size="80" readonly value="true	lille-marseille	city	Lille, France	city	Marseille, France	All	9">
				
				<br>
				<textarea cols="80" rows="5" name="bulkPricingRules" id="bulkPricingRules"></textarea><br/>
				<input type="submit" id="bulkPricingRulesSubmit" value="Go" class="button-primary" name="bulkPricingRulesSubmit" />
				
			</form>
		<?php
	}
}