<?php

Class checkout
{
    function __construct()
    {

        global $woocommerce;

        $distanceHtml = WC()->session->get('distanceHtml');
        $durationHtml = WC()->session->get('durationHtml');
        $estimated_fare = WC()->session->get('estimated_fare');
        $cartypes = WC()->session->get('cartypes');
        $source = WC()->session->get('source');
        $destination = WC()->session->get('destination');
        $car_seats = WC()->session->get('car_seats');
        $child_seats = WC()->session->get('child_seats');
        $infant_seats = WC()->session->get('infant_seats');
        $dateTimePickUp = WC()->session->get('dateTimePickUp');
        $nbToll = WC()->session->get('nbToll');
        $stern_taxi_fare_round_trip = (WC()->session->get('stern_taxi_fare_round_trip') == "true") ? __('Round Trip', 'stern_taxi_fare') : __('One way', 'stern_taxi_fare');
        $dateTimePickUpRoundTrip = WC()->session->get('dateTimePickUpRoundTrip');
        ?>

        <br>
        <div id="my_custom_checkout_field">
            <h3 style="float: none"><?php _e('Details', 'stern_taxi_fare'); ?></h3>
            <?php if (get_option('stern_taxi_use_pets') == 'true') : ?>
                <p class="form-row form-row form-row-wide">
                    <label><?php _e('Number of Pets (if applicable)', 'stern_taxi_fare'); ?></label>
                    <input name="NumberOfPets" id="NumberOfPets" type="number" class="input-text" value="0">
                </p>
            <?php endif; ?>

            <br>

            <h3 style="float: none"><?php _e('Your trip', 'stern_taxi_fare'); ?></h3>

            <p class="form-row form-row form-row-wide">
                <label><?php _e('Pickup Time', 'stern_taxi_fare'); ?></label>
                <input name="dateEstimatedPickedUp" type="text" class="input-text" readonly
                       value="<?php echo $dateTimePickUp; ?>">
                <input name="timeEstimatedPickedUp" type="hidden" class="input-text" readonly value="">
            </p>


            <?php /*
            <p class="form-row form-row form-row-first">
                <label><?php _e('Distance', 'stern_taxi_fare'); ?></label>
                <input type="text" class="input-text" readonly value="<?php echo $distanceHtml; ?>">
            </p>
            <p class="form-row form-row form-row-last">
                <label><?php _e('Duration', 'stern_taxi_fare'); ?></label>
                <input type="text" class="input-text" readonly value="<?php echo $durationHtml; ?>">
            </p>
            */ ?>


            <p class="form-row form-row form-row-wide">
                <label><?php _e('Ticket type', 'stern_taxi_fare'); ?></label>
                <input type="text" class="input-text" readonly value="<?php echo $cartypes; ?>">
            </p>

            <p class="form-row form-row form-row-wide">
                <label><?php _e('Adults', 'stern_taxi_fare'); ?></label>
                <input type="text" class="input-text" readonly value="<?php echo $car_seats; ?>">
            </p>

            <p class="form-row form-row form-row-first">
                <label><?php _e('Children', 'stern_taxi_fare'); ?></label>
                <input type="text" class="input-text" readonly value="<?php echo $child_seats; ?>">
            </p>

            <p class="form-row form-row form-row-last">
                <label><?php _e('Infants', 'stern_taxi_fare'); ?></label>
                <input type="text" class="input-text" readonly value="<?php echo $infant_seats; ?>">
            </p>

            <p class="form-row form-row form-row-wide">
                <label><?php _e('Pickup Location', 'stern_taxi_fare'); ?></label>
                <input type="text" class="input-text" readonly value="<?php echo $source; ?>">
            </p>


            <p class="form-row form-row form-row-wide">
                <label><?php _e('Destination', 'stern_taxi_fare'); ?></label>
                <input type="text" class="input-text" readonly value="<?php echo $destination; ?>">
            </p>

            <p class="form-row form-row form-row-wide">
                <label><?php _e('Round trip?', 'stern_taxi_fare'); ?></label>
                <input type="text" class="input-text" readonly value="<?php echo $stern_taxi_fare_round_trip; ?>">
            </p>

            <?php if ($dateTimePickUpRoundTrip != "") : ?>
                <p class="form-row form-row form-row-wide">
                    <label><?php _e('Pickup Time for Round Trip', 'stern_taxi_fare'); ?></label>
                    <input type="text" class="input-text" readonly value="<?php echo $dateTimePickUpRoundTrip; ?>">
                </p>
            <?php endif; ?>




            <?php


            if (get_option('stern_taxi_fare_show_map_checkout') == true) {
                $apiGoogleKey = get_option('stern_taxi_fare_apiGoogleKey');
                $iframeGmap = "<iframe  width='100%'   height='350' ";
                $iframeGmap .= "frameborder='0' style='border:0'  ";
                $iframeGmap .= "src='https://www.google.com/maps/embed/v1/directions?key=" . $apiGoogleKey;
                $iframeGmap .= "&origin=" . $source;
                $iframeGmap .= "&destination=" . $destination;
                $iframeGmap .= "&avoid=tolls|highways' allowfullscreen></iframe>";
                echo $iframeGmap;
            }
            ?>
        </div>
        <?php
    }
}