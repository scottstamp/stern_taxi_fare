msgid ""
msgstr ""
"Project-Id-Version: stern_taxi_fare\n"
"POT-Creation-Date: 2015-11-29 02:12+0100\n"
"PO-Revision-Date: 2015-11-29 14:21+0100\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en_US\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.11\n"
"X-Poedit-Basepath: ../\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e\n"
"X-Poedit-SearchPath-0: .\n"
"X-Poedit-SearchPathExcluded-0: js\n"

#: classes/widget.php:11 classes/widget.php:14
msgid "Stern taxi fare widget"
msgstr ""

#: classes/widget.php:44
msgid "Booking form"
msgstr ""

#: classes/widget.php:50
msgid "Title:"
msgstr ""

#: stern_taxi_fare_main.php:8 stern_taxi_fare_main.php:147
#: stern_taxi_fare_main.php:167 stern_taxi_fare_main.php:188
msgid "Trip"
msgstr ""

#: stern_taxi_fare_main.php:9 stern_taxi_fare_main.php:148
#: stern_taxi_fare_main.php:168 stern_taxi_fare_main.php:189
#: templates/checkout.php:54
msgid "distance"
msgstr "Distance"

#: stern_taxi_fare_main.php:10 stern_taxi_fare_main.php:149
#: stern_taxi_fare_main.php:169 stern_taxi_fare_main.php:190
#: templates/checkout.php:57
msgid "duration"
msgstr "Duration"

#: stern_taxi_fare_main.php:12 stern_taxi_fare_main.php:151
#: stern_taxi_fare_main.php:171 stern_taxi_fare_main.php:192
#: templates/checkout.php:62
msgid "cartypes"
msgstr "Car types"

#: stern_taxi_fare_main.php:13 stern_taxi_fare_main.php:152
#: stern_taxi_fare_main.php:172 stern_taxi_fare_main.php:193
#: templates/checkout.php:75
msgid "source"
msgstr "Source Address"

#: stern_taxi_fare_main.php:14 stern_taxi_fare_main.php:153
#: stern_taxi_fare_main.php:173 stern_taxi_fare_main.php:194
#: templates/checkout.php:80
msgid "destination"
msgstr "Destination address"

#: stern_taxi_fare_main.php:15 stern_taxi_fare_main.php:154
#: stern_taxi_fare_main.php:174 stern_taxi_fare_main.php:195
#: templates/checkout.php:71
msgid "car_seats"
msgstr "Car seats"

#: stern_taxi_fare_main.php:16 stern_taxi_fare_main.php:155
#: stern_taxi_fare_main.php:175 stern_taxi_fare_main.php:196
msgid "dateTimePickUp"
msgstr "Date Pick up"

#: stern_taxi_fare_main.php:17 stern_taxi_fare_main.php:156
#: stern_taxi_fare_main.php:176 stern_taxi_fare_main.php:197
#: templates/checkout.php:67
msgid "nbToll"
msgstr "Nb Tolls"

#: stern_taxi_fare_main.php:18 stern_taxi_fare_main.php:157
#: stern_taxi_fare_main.php:177 stern_taxi_fare_main.php:198
msgid "stern_taxi_fare_round_trip"
msgstr ""

#: stern_taxi_fare_main.php:19 stern_taxi_fare_main.php:158
#: stern_taxi_fare_main.php:178 stern_taxi_fare_main.php:199
#: templates/checkout.php:88
#, fuzzy
msgid "dateTimePickUpRoundTrip"
msgstr "Date Pick up"

#: stern_taxi_fare_main.php:254
msgid "Please create product \"stern product\""
msgstr ""

#: stern_taxi_fare_main.php:257
msgid "Please install plugin WooCommerce"
msgstr ""

#: templates/admin/design.php:47
msgid "Result"
msgstr ""

#: templates/admin/design.php:55
msgid "Show estimated in result"
msgstr ""

#: templates/admin/design.php:71
msgid "Show distance in result"
msgstr ""

#: templates/admin/design.php:87
msgid "Show duration in result"
msgstr ""

#: templates/admin/design.php:103
msgid "Show Suitcases in result"
msgstr ""

#: templates/admin/design.php:119
msgid "Show tolls in results"
msgstr ""

#: templates/admin/design.php:139
msgid "Use stern_taxi_fare_lib_bootstrap_js "
msgstr ""

#: templates/admin/settings.php:98
msgid "Currency"
msgstr ""

#: templates/admin/settings.php:106
msgid "Language"
msgstr ""

#: templates/admin/settings.php:111
msgid "Country to show"
msgstr ""

#: templates/admin/settings.php:366
msgid "Unit Systems"
msgstr ""

#: templates/admin/settings.php:383
msgid "First date available in hours"
msgstr ""

#: templates/admin/templateRule.php:131
msgid "Bulk upload"
msgstr ""

#: templates/admin/templateRule.php:132
msgid "Use tab to split each colums. 1 rule per row."
msgstr ""

#: templates/admin/templateRule.php:134
msgid "Example:"
msgstr ""

#: templates/checkout.php:18 templates/showForm1.php:89
msgid "Round Trip"
msgstr ""

#: templates/checkout.php:18
msgid "One way"
msgstr ""

#: templates/checkout.php:28
msgid "Your trip"
msgstr ""

#: templates/checkout.php:35 templates/checkout.php:44
msgid "Pickup Time"
msgstr ""

#: templates/checkout.php:84
msgid "Round trip?"
msgstr ""

#: templates/showForm1.php:66
msgid "Type"
msgstr ""

#: templates/showForm1.php:90
msgid "One Way"
msgstr ""

#: templates/showForm1.php:91
msgid "Return"
msgstr ""

#: templates/showForm1.php:100 templates/showForm1.php:106
msgid "Seats"
msgstr ""

#: templates/showForm1.php:142
msgid "Source address"
msgstr ""

#: templates/showForm1.php:155
msgid "Drop Off Address"
msgstr ""

#: templates/showForm1.php:174
msgid "Check Price"
msgstr ""

#: templates/showForm1.php:183
msgid "Show Map"
msgstr ""

#: templates/showForm1.php:189
msgid "Reset Form"
msgstr ""

#: templates/showForm1.php:227
msgid "Great! "
msgstr ""

#: templates/showForm1.php:228
msgid "This is a fixed price ! "
msgstr ""

#: templates/showForm1.php:234
msgid "Error!"
msgstr ""

#: templates/showForm1.php:234
msgid "Please Try again"
msgstr ""

#: templates/showForm1.php:257
#, fuzzy
msgid "Estimated Fare: "
msgstr "Estimated fare"

#: templates/showForm1.php:264
#, fuzzy
msgid "Distance: "
msgstr "Distance"

#: templates/showForm1.php:271
#, fuzzy
msgid "Tolls: "
msgstr "Nb Tolls"

#: templates/showForm1.php:278
#, fuzzy
msgid "Duration: "
msgstr "Duration"

#: templates/showForm1.php:285
msgid "Max suitcases: "
msgstr ""

#: templates/showForm1.php:301 templates/showForm1.php:305
msgid "DateTime Picking"
msgstr ""

#: templates/showForm1.php:321 templates/showForm1.php:325
msgid "DateTime Picking for round trip"
msgstr ""
