��    B      ,  Y   <      �     �     �     �     �     �     �     �     �          %  
   .     9     J  
   S     ^     e     v          �     �     �     �     �     �     �     �     �          "  %   3  !   Y  
   {     �     �  
   �     �     �     �     �     �     �               2     9     H     _     x          �     �     �  %   �  -   �  	   �  	   �     	     	     	     6	     B	     K	     T	     [	     b	  �  }	     3     O     c     q     ~     �     �     �     �     �     �     �     �     �     	       	   !     +     E  
   _     j     q     y     �     �     �     �     �     �  -   �          .  	   4     >     K     X     e     v  ;   �  2   �  /   �  1   #  ;   U     �     �     �     �     �  
   �     �     �     �  .     @   =     ~     �     �     �     �     �     �     �     �                        2      (      )      ?   8       =   /              5   4      	              &                          +   9   :   "         6   $   7                 B       <   *                 -      !             >   ;                                %   
   1       0       @               .       #   3      A   ,   '        Booking form Bulk upload Car type Check Price Country to show Currency DateTime Picking DateTime Picking for round trip Destination Distance Distance:  Drop Off Address Duration Duration:  Error! Estimated Fare:  Example: First date available First date available round trip Great!  Hours Language Max suitcases:  Number of Tolls One Way One way Pickup Time Pickup Time for Round Trip Please Try again Please create product "stern product" Please install plugin WooCommerce Reset Form Result Return Round Trip Round trip? Seats Show Map Show Suitcases in result Show distance in result Show duration in result Show estimated in result Show tolls in results Source Source address Stern taxi fare widget This is a fixed price !  Title: Tolls:  Trip Type Unit Systems Use stern_taxi_fare_lib_bootstrap_js  Use tab to split each colums. 1 rule per row. Your trip car_seats cartypes dateTimePickUp dateTimePickUpRoundTrip destination distance duration nbToll source stern_taxi_fare_round_trip Project-Id-Version: stern_taxi_fare
POT-Creation-Date: 2015-12-02 18:10+0100
PO-Revision-Date: 2015-12-02 18:11+0100
Last-Translator: 
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.11
X-Poedit-Basepath: ../
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: __;_e
X-Poedit-SearchPath-0: .
X-Poedit-SearchPathExcluded-0: js
 Formulaire d'enregistrement Chargement en masse Type vehicule Voir le prix Carte du pays Devise Date Date du retour Adresse de destination Distance Distance :  Adresse de destination Temps estimé Temps estimé :  Erreur! Prix estimé :  Exemple : Première date disponible Première date disponible Excellent! Heures Langage Nombre de bagages :  Nombre de péages Aller simple Aller simple Date Date du retour Merci de réessayer Merci de créer le produit 'stern  taxi fare' Merci d'installer WooCommerce Reset Résultat Aller Retour Aller/retour Aller/retour Nombre de places Voir la carte Voir le nombre de valises dans les résultats du formulaire Voir la distance dans les résultats du formulaire Voir le temps dans les résultats du formulaire Voir le montant dans les résultats du formulaire Voir le nombre de péages dans les résultats du formulaire Adresse initiale Adresse initiale Widget Stern taxi fare  C'est un prix fixe ! Titre Péages :  Voyage Type Systeme d'unité Utiliser la bibliothèque bootstrap javascript Utiliser la tabulation entre chaque colonne. 1 règle par ligne. Votre voyage Places disponibles Type vehicule Date Date du retour Adresse de destination Distance Temps estimé Nombre de Péages Adresse initiale Aller/retour 