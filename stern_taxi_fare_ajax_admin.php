<?php


add_action('wp_ajax_ajax_type_car_admin', 'ajax_type_car_admin');
add_action('wp_ajax_nopriv_ajax_type_car_admin', 'ajax_type_car_admin');
function ajax_type_car_admin()
{

    if (isset($_POST['id']) && isset($_POST['carFare'])) {
        $id = $_POST['id'];
        $carFare = $_POST['carFare'];
        $otypeCar = new typeCar($id);
        $otypeCar->setcarFare($carFare);
        $otypeCar->save();
    }
    if (isset($_POST['arrayOrder'])) {
        $arrayOrder = $_POST['arrayOrder'];
        foreach ($arrayOrder as $value) {
            echo "<script>console.log( 'Debug Objects: " . $value[0] . "/" . $value[1] . "' );</script>";
            $otypeCar = new typeCar($value[1]);
            $otypeCar->setorganizedBy($value[0]);
            $otypeCar->save();
            $returnData = array($id, $organizedBy);

        }
    }
    if (isset($_POST['id']) && isset($_POST['carType'])) {
        $id = $_POST['id'];
        $carType = $_POST['carType'];
        $otypeCar = new typeCar($id);
        $otypeCar->setcarType($carType);
        $otypeCar->save();
    }
    if (isset($_POST['id']) && isset($_POST['carSeat'])) {
        $id = $_POST['id'];
        $carSeat = $_POST['carSeat'];
        $otypeCar = new typeCar($id);
        $otypeCar->setcarSeat($carSeat);
        $otypeCar->save();
    }
    if (isset($_POST['id']) && isset($_POST['childSeat'])) {
        $id = $_POST['id'];
        $childSeat = $_POST['childSeat'];
        $otypeCar = new typeCar($id);
        $otypeCar->setchildSeat($childSeat);
        $otypeCar->save();
    }
    if (isset($_POST['id']) && isset($_POST['infantSeat'])) {
        $id = $_POST['id'];
        $infantSeat = $_POST['infantSeat'];
        $otypeCar = new typeCar($id);
        $otypeCar->setinfantSeat($infantSeat);
        $otypeCar->save();
    }
    if (isset($_POST['id']) && isset($_POST['suitcases'])) {
        $id = $_POST['id'];
        $suitcases = $_POST['suitcases'];
        $otypeCar = new typeCar($id);
        $otypeCar->setsuitcases($suitcases);
        $otypeCar->save();
    }
    if (isset($_POST['id']) && isset($_POST['farePerDistance'])) {
        $id = $_POST['id'];
        $farePerDistance = $_POST['farePerDistance'];
        $otypeCar = new typeCar($id);
        $otypeCar->setfarePerDistance($farePerDistance);
        $otypeCar->save();
    }
    if (isset($_POST['id']) && isset($_POST['farePerMinute'])) {
        $id = $_POST['id'];
        $farePerMinute = $_POST['farePerMinute'];
        $otypeCar = new typeCar($id);
        $otypeCar->setfarePerMinute($farePerMinute);
        $otypeCar->save();
    }
    if (isset($_POST['id']) && isset($_POST['farePerSeat'])) {
        $id = $_POST['id'];
        $farePerSeat = $_POST['farePerSeat'];
        $otypeCar = new typeCar($id);
        $otypeCar->setfarePerSeat($farePerSeat);
        $otypeCar->save();
    }
    if (isset($_POST['id']) && isset($_POST['farePerToll'])) {
        $id = $_POST['id'];
        $farePerToll = $_POST['farePerToll'];
        $otypeCar = new typeCar($id);
        $otypeCar->setfarePerToll($farePerToll);
        $otypeCar->save();
    }
    if (isset($_POST['id']) && isset($_POST['isDelete'])) {
        $id = $_POST['id'];
        $otypeCar = new typeCar($id);
        $otypeCar->delete();
    }
    if (isset($_POST['isNewTypeCar'])) {
        $otypeCar = new typeCar();
        $typeCarId = $otypeCar->save();
        $response = $typeCarId;
        echo json_encode($response);
    }
    if (isset($_POST['loadInit'])) {
        $args = array(
            'post_type' => 'stern_taxi_car_type',
            'posts_per_page' => 200,
            'order' => 'ASC',
            'orderby' => 'meta_value',
            'meta_key' => '_stern_taxi_car_type_organizedBy'
        );

        $allPosts = get_posts($args);

        foreach ($allPosts as $post) {
            setup_postdata($post);
            $otypeCar = new typeCar($post->ID);


            $arrayData[] =
                array(
                    'id' => $otypeCar->getid(),
                    'carType' => $otypeCar->getcarType(),
                    'carFare' => $otypeCar->getcarFare(),
                    'carSeat' => $otypeCar->getcarSeat(),
                    'childSeat' => $otypeCar->getchildSeat(),
                    'infantSeat' => $otypeCar->getinfantSeat(),
                    'suitcases' => $otypeCar->getsuitcases(),
                    'organizedBy' => $otypeCar->getorganizedBy(),
                    'farePerDistance' => $otypeCar->getfarePerDistance(),
                    'farePerMinute' => $otypeCar->getfarePerMinute(),
                    'farePerSeat' => $otypeCar->getfarePerSeat(),
                    'farePerToll' => $otypeCar->getfarePerToll(),

                );

        }
        $response = $arrayData;
        echo json_encode($response);
    }
    wp_die();
}


