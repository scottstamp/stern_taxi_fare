var initialLoad = true;
//var taxi_type;
var stops_count = 0;
var baby_count = 0;
var child_count = 0;
var infants_count = 0;
var taxi_type_m;
var taxi_type_s;
var markerBounds = new google.maps.LatLngBounds();
var geocoder = new google.maps.Geocoder();
var markers = new Array();
var directionsDisplay;
var directionsService;
var map;
var lats = '';
var lngs = '';
//count_markers = 0;


//var waypoints = new Array();


jQuery(document).ready(function () {
    if (document.getElementById('countryHidden') != null) {

        var VarCountry = document.getElementById('countryHidden').value;
        var options = {
            componentRestrictions: {country: VarCountry}
        };

        // code for google auto suggestion address for pick up location
        var input = document.getElementById('source');
        var autocomplete = new google.maps.places.Autocomplete(input, options);

        // code for Google auto suggestion address for destination location
        var drop = document.getElementById('destination');
        var drop_autocomplete = new google.maps.places.Autocomplete(drop, options);

    }

    if (initialLoad == true) {
        setNowDate();
    }
    initialLoad = false;

});


jQuery(document).ready(function () {
    jQuery('[data-toggle="tooltip"]').tooltip();

    jQuery('#stern_taxi_fare_div')
        .bootstrapValidator({
            message: 'This value is not valid',
            feedbackIcons: {
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            },
            fields: {
                source: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        }
                    }
                },
                destination: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        }
                    }
                },
                dateTimePickUp: {
                    validators: {
                        notEmpty: {
                            message: ' '
                        }
                    }
                }
            }
        });


    jQuery('#resetBtn').click(function () {
        softReset();
        document.getElementById('SpanCal1').className = "glyphicon glyphicon-check";
        jQuery('#stern_taxi_fare_div').data('bootstrapValidator').resetForm(true);
        jQuery("#resultLeft").css("display", "none");
        jQuery("#resultText").css("display", "none");
        document.getElementById('cal3').style.visibility = 'hidden';
        setNowDate();
    });


    jQuery('#cal1').click(function () {
        jQuery('#stern_taxi_fare_div').bootstrapValidator('validate');
        //	document.getElementById( "cal1" ).setAttribute( "onClick", "doCalculation()" );
        document.getElementById('SpanCal3').className = "glyphicon glyphicon-map-marker";
    });


    /*
     jQuery('#destination').on('change', function(){
     var selectedCarTypeId = jQuery(this).find("option:selected").val();

     });
     */

    var lang = document.documentElement.lang;

    jQuery('#dateTimePickUp').datetimepicker({
        showClose: true,
        minDate: startDate,
        locale: lang
    });


    jQuery('#dateTimePickUpRoundTrip').datetimepicker({
        showClose: true,
        minDate: startDate,
        locale: lang,
    });


    jQuery('#baby_count').on('change', function () {
        softReset();
    });

    jQuery('#stern_taxi_fare_round_trip').on('change', function () {
        softReset();
        var Selectstern_taxi_fare_round_trip = document.getElementById('stern_taxi_fare_round_trip').value;
        if (Selectstern_taxi_fare_round_trip == "false") {
            jQuery("#divDateTimePickUpRoundTrip").css("display", "none");
            if (document.getElementById('dateTimePickUpRoundTrip') != null) {
                document.getElementById("dateTimePickUpRoundTrip").value = "";
            }
        } else {
            jQuery("#divDateTimePickUpRoundTrip").css("display", "");
            setNowDateRoundTrip()
        }
    });


    jQuery('#cartypes').on('change', function () {
        refreshSeats();
    });


    //var lang = document.documentElement.lang;
    //var get24Or12hr ='';
    //if(document.getElementById('get24Or12hr') !=null){
    //	var get24Or12hr = document.getElementById("get24Or12hr").value
    //}

    if (document.getElementById("First_date_available_in_hours") != null) {
        var First_date_available_in_hours = document.getElementById("First_date_available_in_hours").value
        var startDate = new Date();
        startDate.setTime(startDate.getTime() + First_date_available_in_hours * (1000 * 60 * 60));
    }


    /*


     jQuery('#datetimepicker8').datetimepicker({
     format: 'LT',
     disabledHours: [
     "8"
     ]

     });

     */


});

function closeBox() {
    document.getElementById('boxclose').style.visibility = 'hidden';
    setTimeout(function () {
        document.getElementById('main2').style.visibility = 'hidden';
        document.getElementById('SpanCal3').className = "glyphicon glyphicon-map-marker";
    }, 0);

}

function setNowDate() {
    if (document.getElementById("First_date_available_in_hours") != null) {
        var First_date_available_in_hours = document.getElementById("First_date_available_in_hours").value
        var startDate = new Date();
        startDate.setTime(startDate.getTime() + First_date_available_in_hours * (1000 * 60 * 60));


        var options = {
            year: 'numeric',
            month: '2-digit',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            hour12: false
        };
        if (document.getElementById('dateTimePickUp') != null) {
            document.getElementById("dateTimePickUp").value = startDate.toLocaleString(document.documentElement.lang, options);
        }
    }
}

function setNowDateRoundTrip() {


    if (document.getElementById("First_date_available_roundtrip_in_hours") != null) {
        var First_date_available_roundtrip_in_hours = document.getElementById("First_date_available_roundtrip_in_hours").value
        var startDate = new Date();
        startDate.setTime(startDate.getTime() + (First_date_available_roundtrip_in_hours ) * (1000 * 60 * 60));


        var options = {
            year: 'numeric',
            month: '2-digit',
            day: 'numeric',
            hour: 'numeric',
            minute: 'numeric',
            hour12: false
        };
        if (document.getElementById('dateTimePickUpRoundTrip') != null) {
            document.getElementById("dateTimePickUpRoundTrip").value = startDate.toLocaleString(document.documentElement.lang, options);
        }
    }
}

function refreshSeats() {
    var selectedCarTypeId = document.getElementById('cartypes').value;
    softReset();
    var dataPicker = {
        'action': 'my_ajax_picker',
        'selectedCarTypeId': selectedCarTypeId
    };

    jQuery("#labelSeats").empty();
    jQuery("#labelSeats").append("<option data-icon='glyphicon-refresh glyphicon-spin' value='' selected>  </option>");
    jQuery('.selectpicker').selectpicker('refresh');

    /*
     if(document.getElementById('buttonDateTime')!=null) {
     document.getElementById('buttonDateTime').className="glyphicon glyphicon-refresh glyphicon-spin";
     }
     */

    jQuery.post(my_ajax_object_picker.ajax_url, dataPicker, function (DataResponse) {
        //console.log(DataResponse);
        var cadena = '';
        var childSeats = '';
        var infantSeats = '';
        var selected = '';
        var res = jQuery.parseJSON(DataResponse);
        for (i = 0; i <= res.carseat; i++) {
            if (i == 1) var selected = 'selected';
            else var selected = '';

            cadena += "<option data-icon='glyphicon-user' value='" + i + "' " + selected + ">  " + i + "</option>";
        }

        for (i = 0; i <= res.childseat; i++) {
            if (i == 0) var selected = 'selected';
            else var selected = '';

            childSeats += "<option data-icon='glyphicon-user' value='" + i + "' " + selected + ">  " + i + "</option>";
        }

        for (i = 0; i <= res.infantseat; i++) {
            if (i == 0) var selected = 'selected';
            else var selected = '';

            infantSeats += "<option data-icon='glyphicon-user' value='" + i + "' " + selected + ">  " + i + "</option>";
        }

        var adultSeatsGroup = jQuery("#labelSeats");
        adultSeatsGroup.empty();
        adultSeatsGroup.append(cadena);

        var childSeatsGroup = jQuery("#labelChildSeats");
        childSeatsGroup.empty();
        childSeatsGroup.append(childSeats);

        var infantSeatsGroup = jQuery("#labelInfantSeats");
        infantSeatsGroup.empty();
        infantSeatsGroup.append(infantSeats);

        jQuery('.selectpicker').selectpicker('refresh');
    });
}


function refreshDates(duration) {
    var selectedCarTypeId = document.getElementById('cartypes').value;


    var dataPicker = {
        'action': 'my_ajax_picker',
        'selectedCarTypeId': selectedCarTypeId
    };


    jQuery.post(my_ajax_object_picker.ajax_url, dataPicker, function (DataResponse) {

        var res = jQuery.parseJSON(DataResponse);
        var stern_taxi_fare_Time_To_add_after_a_ride = document.getElementById('stern_taxi_fare_Time_To_add_after_a_ride').value;
        var disabledDatesTimeArray = []
        var arrayCalendarJS = res.arrayCalendar;
        if (arrayCalendarJS != '') {

            var arrayCalendarMoment = [];
            var arrayDisableDates = [];

            for (var key in arrayCalendarJS) {
                var val = arrayCalendarJS[key];
                //console.log(val["dateTimeBegin"]);
                //var dateTimeBegin = moment(val["dateBegin"]+" " +val["dateTimeBegin"]);
                //var dateTimeEnd = moment(val["dateEnd"]+" " +val["dateTimeEnd"]);

                dateBeginWithpotentialBooking = new Date(val["dateTimeBegin"]);
                dateBeginWithpotentialBooking = new Date(dateBeginWithpotentialBooking.getTime() - duration * (60 * 1000) - stern_taxi_fare_Time_To_add_after_a_ride * (60 * 1000));

                dateTimeBegin = moment(dateBeginWithpotentialBooking);
                dateTimeEnd = moment(val["dateTimeEnd"]);

                console.log(dateTimeBegin);
                console.log(dateTimeEnd);

                arrayCalendarMoment.push([
                    dateTimeBegin,
                    dateTimeEnd
                ]);
                var nbDaysinPeriod = ((dateTimeBegin - dateTimeEnd) / (1000 * 60 * 60 * 24));
                dateParsing = new Date(val["dateTimeBegin"]);
                console.log(dateParsing);
                for (k = 0; k < nbDaysinPeriod - 1; k++) {

                    dateParsing.setTime(dateParsing.getTime() + 1 * (24 * 60 * 60 * 1000));
                    //console.log(dateParsing);
                    arrayDisableDates.push(moment(dateParsing));

                }

            }
            /*
             dateEnd = new Date(dateTimePickUp);
             dateEnd = new Date(dateEnd.getTime() + duration*60000);
             arrayCalendarMoment.push([
             moment(dateTimePickUp),
             moment(dateTimePickUp).add(duration,'minutes' )
             ]);


             console.log(dateTimePickUp);
             console.log(dateEnd);
             console.log(duration);
             */
            startDate = getStartDateForDateTimepicker()


            var lang = document.documentElement.lang;

            if (document.getElementById("stern_taxi_fare_use_calendar").value == 'true') {

                jQuery('#dateTimePickUp').data("DateTimePicker").destroy();
                jQuery('#dateTimePickUp').datetimepicker({
                    showClose: true,
                    locale: lang,
                    minDate: startDate,
                    disabledDates: arrayDisableDates,
                    disabledTimeIntervals: arrayCalendarMoment

                });
            }
        }
        if (document.getElementById('buttonDateTime') != null) {
            document.getElementById('buttonDateTime').className = "glyphicon glyphicon-time";
        }
    });
}


function getStartDateForDateTimepicker() {
    if (document.getElementById("First_date_available_in_hours") != null) {
        var First_date_available_in_hours = document.getElementById("First_date_available_in_hours").value
        var startDate = new Date();
        startDate.setTime(startDate.getTime() + First_date_available_in_hours * (1000 * 60 * 60));
        return startDate;

    }
}


function softReset() {
    document.getElementById('cal3').style.visibility = 'hidden';
    document.getElementById('main2').style.visibility = 'hidden';
    jQuery("#divAlert").css("display", "none");
    jQuery("#divAlertError").css("display", "none");
    jQuery("#resultText").css("display", "none");

    jQuery("#resultLeft").css("display", "none");
    if (document.getElementById('boxclose') != null) {
        document.getElementById('boxclose').style.visibility = 'hidden';
    }
    //document.getElementById("resultText").innerHTML = "";
}

function getLocation() {
    document.getElementById('getLocationSource').className = "glyphicon glyphicon-refresh glyphicon-spin";
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(onGeoSuccess);
    }
    softReset();
}

function onGeoSuccess(event) {
    geocoder = new google.maps.Geocoder();
    codeLatLng(event.coords.latitude, event.coords.longitude);
}

var geocoder;

function codeLatLng(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({
        'latLng': latlng
    }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                //console.log(results[1]);
                document.getElementById("source").value = results[0].formatted_address;
            } else {
                alert('No results found');
            }
        } else {
            alert('Geocoder failed due to: ' + status);
        }
        document.getElementById('getLocationSource').className = "glyphicon glyphicon-map-marker";
    });
}


function getLocationDestination() {

    //var stern_taxi_fare_saved_point_to_use_in_button_destination = document.getElementById("stern_taxi_fare_saved_point_to_use_in_button_destination").value;
    var stern_taxi_fare_address_saved_point = document.getElementById("stern_taxi_fare_address_saved_point").value;
    var stern_taxi_fare_address_saved_point2 = document.getElementById("stern_taxi_fare_address_saved_point2").value;

    if (stern_taxi_fare_address_saved_point != '') {
        if (document.getElementById("destination").value != stern_taxi_fare_address_saved_point) {
            document.getElementById("destination").value = stern_taxi_fare_address_saved_point;
        } else if (stern_taxi_fare_address_saved_point2 != "") {
            document.getElementById("destination").value = stern_taxi_fare_address_saved_point2;
        }


    } else {
        document.getElementById('getLocationDestination').className = "glyphicon glyphicon-refresh glyphicon-spin";
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(onGeoSuccessDestination);
        }
    }
    softReset();
}

function onGeoSuccessDestination(event) {
    geocoder = new google.maps.Geocoder();
    codeLatLngDestination(event.coords.latitude, event.coords.longitude);
}

var geocoder;

function codeLatLngDestination(lat, lng) {
    var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({
        'latLng': latlng
    }, function (results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            if (results[0]) {
                //console.log(results[1]);
                document.getElementById("destination").value = results[0].formatted_address;
            } else {
                alert('No results found');
            }
        } else {
            alert('Geocoder failed due to: ' + status);
        }
        document.getElementById('getLocationDestination').className = "glyphicon glyphicon-map-marker";
    });
}


function showMap() {
    if (document.getElementById('main2').style.visibility == 'hidden') {

        document.getElementById('SpanCal3').className = "glyphicon glyphicon-eye-close";
        var source = document.getElementById("source").value;
        var destination = document.getElementById("destination").value;
        var apiGoogleKey = document.getElementById("apiGoogleKey").value;
        var getKmOrMilesHTML = '';
        var getKmOrMiles = document.getElementById("getKmOrMiles").value;
        if (getKmOrMiles == 'km') {
            getKmOrMilesHTML = 'metric';
        } else {
            getKmOrMilesHTML = 'imperial';
        }


        jQuery("#main2").html("");
        document.getElementById('main2').style.visibility = 'visible';

        var stern_taxi_fare_avoid_highways_in_calculation = document.getElementById('stern_taxi_fare_avoid_highways_in_calculation').value;

        $htmliframe = "";
        $htmliframe += "<a class='boxclose' id='boxclose' onclick='closeBox();'></a>";
        $htmliframe += "<iframe  width='100%'   height='450' ";
        $htmliframe += "frameborder='0' style='border:0'  ";
        $htmliframe += "src='https://www.google.com/maps/embed/v1/directions?key=" + apiGoogleKey;
        $htmliframe += "&origin=" + source;
        $htmliframe += "&units=" + getKmOrMilesHTML;
        $htmliframe += "&mode=driving";
        if (stern_taxi_fare_avoid_highways_in_calculation == "true") {
            $htmliframe += "&avoid=highways";
        }
        $htmliframe += "&destination=" + destination;
        $htmliframe += "' allowfullscreen></iframe>";
        jQuery("#main2").append($htmliframe);

    } else {
        document.getElementById('main2').style.visibility = 'hidden';
        document.getElementById('SpanCal3').className = "glyphicon glyphicon-map-marker";
    }

}


function doCalculation() {
    document.getElementById('cal3').style.visibility = 'hidden';
    softReset();
    var cartypes = document.getElementById('cartypes').value;
    var address = document.getElementById('source').value;
    var destination = document.getElementById('destination').value;
//	var dateTimePickUp = document.getElementById('dateTimePickUp').value;


    if (cartypes.trim() == '') {
        return false;
    }
    else if (address.trim() == '') {
        source = '';
        return false;
    }
    else if (destination.trim() == '') {
        destination = '';
        return false;
    }


    else {
        calc();


    }

}
function calc() {
    document.getElementById('SpanCal1').className = "glyphicon glyphicon-refresh glyphicon-spin";


    var source = document.getElementById("source").value;
    var stops_counts = document.getElementById("stops_count").value;
    var destination = document.getElementById("destination").value;
    var e = document.getElementById('cartypes');
    var cartypes = e.options[e.selectedIndex].text

    //var stops_count = document.getElementById("stops_count").value;
    //var minutefare =document.getElementById('minutefare').value;
    //var stopfare=document.getElementById('stopfare').value;
    //var milefare=document.getElementById('milefare').value;
    //var tollfare=document.getElementById('stern_taxi_fare_Tolls').value;
    //var seatfare=document.getElementById('seatfare').value;
//      var car_type_carfare=document.getElementById('car_type_carfare').value;
//		var curr=document.getElementById('currfare').value;
    //var fare_minimum=document.getElementById('stern_taxi_fare_minimum').value;
    //var stern_taxi_fare_fixed_amount=document.getElementById('stern_taxi_fare_fixed_amount').value;

    var dateTimePickUp = document.getElementById('dateTimePickUp').value;
    var dateTimePickUpRoundTrip = document.getElementById('dateTimePickUpRoundTrip').value;
    var currency_symbol = document.getElementById('currency_symbol').value;
    var selectedCarTypeId = document.getElementById('cartypes').value;
    var stern_taxi_fare_round_trip = document.getElementById('stern_taxi_fare_round_trip').value;

    var currency_symbol_right = '';
    var currency_symbol_left = '';
    if (currency_symbol == '€') {
        currency_symbol_right = currency_symbol;
        currency_symbol_left = '';
    } else {
        currency_symbol_right = '';
        currency_symbol_left = currency_symbol;
    }


    if (document.getElementById("baby_count")) {
        baby_count = parseFloat(document.getElementById("baby_count").value);
    }

    if (document.getElementById("child_count")) {
        child_count = parseFloat(document.getElementById("child_count").value);
    }

    if (document.getElementById("infants_count")) {
        infants_count = parseFloat(document.getElementById("infants_count").value);
    }

    var getShow_use_img_gif_loader = document.getElementById("getShow_use_img_gif_loader").value;
    var getKmOrMiles = document.getElementById("getKmOrMiles").value;
    //	var suitcases = document.getElementById("suitcases").value;
//		var stern_taxi_fare_show_suitcases_in_form = document.getElementById("stern_taxi_fare_show_suitcases_in_form").value;


    var stern_taxi_fare_url_gif_loader = document.getElementById("stern_taxi_fare_url_gif_loader").value;


    var dataToll = {
        'action': 'my_ajax_toll',
        'source': source,
        'destination': destination
    };

    //console.log('as ' + dateTimePickUp);
    //dateTimePickUp =  Date(dateTimePickUp);

    /*
     moment.lang(navigator.language);
     var dateLocale = moment(dateTimePickUp);
     var dateSQL = moment(dateLocale).format("YYYY-MM-YY hh:mm:ss");
     console.log( dateLocale + " " + dateSQL);
     */

    var data = {
        'action': 'my_ajax',
        'cartypes': cartypes,
        'source': source,
        'selectedCarTypeId': selectedCarTypeId,
        'destination': destination,
        'car_seats': baby_count,
        'child_seats': child_count,
        'infant_seats': infants_count,
        'stern_taxi_fare_round_trip': stern_taxi_fare_round_trip,
        'dateTimePickUpRoundTrip': dateTimePickUpRoundTrip,
        'dateTimePickUp': dateTimePickUp
    };
    jQuery.post(my_ajax_object.ajax_url, data, function (response) {
        //jQuery("#resultText").css("display","");
        jQuery("#resultLeft").css("display", "none");

        /*
         if(getShow_use_img_gif_loader=='true') {
         document.getElementById("resultText").innerHTML = "<img src='" + stern_taxi_fare_url_gif_loader + "'>";
         } else {
         document.getElementById("resultText").innerHTML = "";
         }
         */

        var stern_taxi_fare_show_map = document.getElementById('stern_taxi_fare_show_map').value;
        if (stern_taxi_fare_show_map != 'false') {
            document.getElementById('cal3').style.visibility = 'visible';
        }
        document.getElementById('SpanCal1').className = "glyphicon glyphicon-refresh";


        /*
         var stern_taxi_fare_text_for_Estimated_Fare = document.getElementById("stern_taxi_fare_text_for_Estimated_Fare").value;
         var stern_taxi_fare_text_for_Distance = document.getElementById("stern_taxi_fare_text_for_Distance").value;
         var stern_taxi_fare_text_for_Tolls = document.getElementById("stern_taxi_fare_text_for_Tolls").value;
         var stern_taxi_fare_text_for_Duration = document.getElementById("stern_taxi_fare_text_for_Duration").value;
         var stern_taxi_fare_text_for_Max_suitcases = document.getElementById("stern_taxi_fare_text_for_Max_suitcases").value;
         */


        console.log(response);

        var res = jQuery.parseJSON(response);
        if (res.statusGoogleGlobal == "errorGoogleEmpty") {
            jQuery("#divAlertError").css("display", "");
            //document.getElementById("divAlertErrorText").innerHTML = "<strong>Error!</strong> Please Try again";

        } else {
            if (res.RuleApproved == true) {
                jQuery("#divAlert").css("display", "");
                var stern_taxi_fare_great_text = document.getElementById("stern_taxi_fare_great_text").value;
                var stern_taxi_fare_fixed_price_text = document.getElementById("stern_taxi_fare_fixed_price_text").value;
                document.getElementById("divAlertText").innerHTML = "<strong>" + stern_taxi_fare_great_text + " </strong>" + stern_taxi_fare_fixed_price_text + " " + res.nameRule;
            }

            jQuery("#resultLeft").css("display", "");
            jQuery("#resultText").css("display", "");

            if (document.getElementById("estimatedFareSpanValue") != null) {
                document.getElementById("estimatedFareSpanValue").innerHTML = currency_symbol_left + res.estimated_fare + currency_symbol_right;
            }
            if (document.getElementById("distanceSpanValue") != null) {
                document.getElementById("distanceSpanValue").innerHTML = res.distanceHtml;
            }
            if (document.getElementById("tollSpanValue") != null) {
                document.getElementById("tollSpanValue").innerHTML = res.nbToll;
            }
            if (document.getElementById("durationSpanValue") != null) {
                document.getElementById("durationSpanValue").innerHTML = res.durationHtml;
            }
            if (document.getElementById("suitcasesSpanValue") != null) {
                document.getElementById("suitcasesSpanValue").innerHTML = res.suitcases;
            }

            refreshDates(res.duration);


            if (document.getElementById('stern_taxi_fare_auto_open_map').value == 'true') {
                showMap();
            }

        }

    });


}


function checkout_url_function() {
    document.getElementById('SpanBookButton').className = "glyphicon glyphicon-refresh glyphicon-spin";
    var stern_taxi_fare_round_trip = document.getElementById('stern_taxi_fare_round_trip').value;
    var dateTimePickUpRoundTrip = document.getElementById('dateTimePickUpRoundTrip').value;
    var dateTimePickUp = document.getElementById('dateTimePickUp').value;
    var data = {
        'action': 'my_ajax',
        'updateDateSection': 'updateDateSection',
        'stern_taxi_fare_round_trip': stern_taxi_fare_round_trip,
        'dateTimePickUpRoundTrip': dateTimePickUpRoundTrip,
        'dateTimePickUp': dateTimePickUp
    };
    jQuery.post(my_ajax_object.ajax_url, data, function (response) {

        var checkout_url_var = '';
        checkout_url_var = document.getElementById('checkout_url').value;
        window.location = checkout_url_var;
    });
}

