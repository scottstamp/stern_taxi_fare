<?php


add_action('wp_ajax_my_ajax_picker', 'my_ajax_picker');
add_action('wp_ajax_nopriv_my_ajax_picker', 'my_ajax_picker');
function my_ajax_picker()
{
    $arrayCalendar = '';
    $selectedCarTypeId = $_POST['selectedCarTypeId'];
    $carseat = get_post_meta($selectedCarTypeId, '_stern_taxi_car_type_carseat', true);
    $childseat = get_post_meta($selectedCarTypeId, '_stern_taxi_car_type_childseat', true);
    $infantseat = get_post_meta($selectedCarTypeId, '_stern_taxi_car_type_infantseat', true);

    $selectedCarTypeId = $selectedCarTypeId;
    $args = array(
        'post_type' => 'stern_taxi_calendar',
        'nopaging' => true,
        'meta_query' => array(
            array(
                'key' => 'typeIdCar',
                'value' => $selectedCarTypeId,
                'compare' => '=',
            ),
        ),

    );

    $allPosts = get_posts($args);
    foreach ($allPosts as $post) {
        setup_postdata($post);
        $oCalendar = new calendar($post->ID);
        $arrayCalendar[$post->ID]["typeCalendar"] = $oCalendar->gettypeCalendar();
        //$arrayCalendar[$post->ID]["dateBegin"] = $oCalendar->getdateBegin();
        $arrayCalendar[$post->ID]["dateTimeBegin"] = $oCalendar->getdateTimeBegin();
        //$arrayCalendar[$post->ID]["dateEnd"] = $oCalendar->getdateEnd();
        $arrayCalendar[$post->ID]["dateTimeEnd"] = $oCalendar->getdateTimeEnd();

    }


    $response["carseat"] = $carseat;
    $response["childseat"] = $childseat;
    $response["infantseat"] = $infantseat;
    $response["arrayCalendar"] = $arrayCalendar;


    echo json_encode($response);
    wp_die();
}

add_action('wp_ajax_my_ajax', 'my_ajax');
add_action('wp_ajax_nopriv_my_ajax', 'my_ajax');
function my_ajax()
{
    if (isset($_POST['dateTimePickUp']) && isset($_POST['updateDateSection'])) {
        $dateTimePickUp = ($_POST['dateTimePickUp']);
        $stern_taxi_fare_round_trip = ($_POST['stern_taxi_fare_round_trip']);
        $dateTimePickUpRoundTrip = ($_POST['dateTimePickUpRoundTrip']);


        global $woocommerce;

        WC()->session->set('dateTimePickUp', $dateTimePickUp);
        WC()->session->set('stern_taxi_fare_round_trip', $stern_taxi_fare_round_trip);
        WC()->session->set('dateTimePickUpRoundTrip', $dateTimePickUpRoundTrip);

    }

    if (isset($_POST['cartypes']) && isset($_POST['source']) && isset($_POST['destination']) && isset($_POST['car_seats']) && isset($_POST['child_seats']) && isset($_POST['infant_seats']) && isset($_POST['dateTimePickUp']) && isset($_POST['selectedCarTypeId'])) {
        global $wpdb;

        //$distance 						= intval( $_POST['distance'] );
        //	$duration 						=  $_POST['duration'] ;
        //	$estimated_fare 				= intval( $_POST['estimated_fare'] );
        $cartypes = ($_POST['cartypes']);
        $source = ($_POST['source']);
        $destination = ($_POST['destination']);
        $car_seats = ($_POST['car_seats']);
        $child_seats = ($_POST['child_seats']);
        $infant_seats = ($_POST['infant_seats']);
        $dateTimePickUp = ($_POST['dateTimePickUp']);
        $selectedCarTypeId = ($_POST['selectedCarTypeId']);
        $stern_taxi_fare_round_trip = ($_POST['stern_taxi_fare_round_trip']);
        $dateTimePickUpRoundTrip = ($_POST['dateTimePickUpRoundTrip']);

        /*
            $km_fare 						= get_option('stern_taxi_fare_mile');
            $minutefare 					= get_option('stern_taxi_fare_minute');
            $seatfare 						= get_option('stern_taxi_fare_seat');
            $tollfare 						= getFareToll();
        */
        $fare_minimum = getFareMinimum();
        $stern_taxi_fare_fixed_amount = get_option('stern_taxi_fare_fixed_amount');
        $estimated_fare = 0;
        $durationHtml = "";
        $GoogleMapsData = getGoogleMapsDataFunction($source, $destination);
        $GoogleMapsDataStatus = $GoogleMapsData["status"];
        if ($GoogleMapsDataStatus != "errorGoogleEmpty") {
            $distance = $GoogleMapsData["DistanceValueKMOrMiles"];
            $distanceHtml = $GoogleMapsData["DistanceText"];
            $duration = $GoogleMapsData["DurationValue"];
            $durationHtml = $GoogleMapsData["DurationText"];
            $nbToll = getCountToll($source, $destination);
            //	$suitcases = get_post_meta($selectedCarTypeId ,'_stern_taxi_car_type_suitcases',true);

            $otypeCar = new typeCar($selectedCarTypeId);
            $carFare = $otypeCar->getcarFare();
            $suitcases = $otypeCar->getsuitcases();
            $km_fare = $otypeCar->getfarePerDistance();
            $minutefare = $otypeCar->getfarePerMinute();
            $seatfare = $otypeCar->getfarePerSeat();
            $tollfare = $otypeCar->getfarePerToll();


            $addressSavedPoint = get_option('stern_taxi_fare_address_saved_point');

            $priceRule = 0;
            $idRule = 0;
            //	$nbMaxRuleBeforeGoogleStop=0;
            $RuleApproved = false;
            $SourceRuleApproved = "";
            $DestinationRuleApproved = "";
            $typeIdCarInRule = -1;

            $carTypeRuleApproved = false;
            //	$typeCalculation = get_option('typeCalculation');
            $nameRule = "";
            $logParsingRule = "";
            //$city = getCityFromAddress($source);


            $args = array(
                'post_type' => 'stern_taxi_rule',
                'nopaging' => true,
                /*'meta_query' => array(
                    array(
                    'key' => 'typeIdCar',
                    'value' => $selectedCarTypeId,
                    'compare' => '='
                    )
                )*/
            );

            $the_query = new WP_Query($args);
            while ($the_query->have_posts()) {
                $oRule = null;
                $the_query->the_post();
                $typeRuleSource = "";
                $typeRuleDest = "";

                //$allRules = get_posts( $args );
                //foreach ( $allRules as $post )  {
                //setup_postdata( $post );
                //	$nbMaxRuleBeforeGoogleStop++;
                $oRule = new rule($the_query->post->ID);
                //var_dump ($oRule);


                if ($oRule->getisActive() == 'true') {
                    $SourceRuleApproved = "";
                    $DestinationRuleApproved = "";


                    $typeIdCarInRule = $oRule->gettypeIdCar();

                    if ($typeIdCarInRule == "" or ($selectedCarTypeId == $typeIdCarInRule)) {

                        $address1 = $oRule->gettypeSourceValue();
                        $address2 = $source;
                        $typeRuleSource = $oRule->gettypeSource();
                        $SourceRuleApproved = checkIfRuleIsApproved($address1, $address2, $typeRuleSource);

                        if ($SourceRuleApproved == "errorGoogleEmpty") {
                            $logParsingRule = $logParsingRule . "," . $oRule->getid() . "(" . $SourceRuleApproved . "[" . $typeRuleSource . "]," . $DestinationRuleApproved . "[" . $typeRuleDest . "])";
                            break;
                        }

                        if ($SourceRuleApproved == "ok") {

                            $address1 = $oRule->gettypeDestinationValue();
                            $address2 = $destination;
                            $typeRuleDest = $oRule->gettypeDestination();
                            $DestinationRuleApproved = checkIfRuleIsApproved($address1, $address2, $typeRuleDest);

                            if ($DestinationRuleApproved == "errorGoogleEmpty") {
                                $logParsingRule = $logParsingRule . "," . $oRule->getid() . "(" . $SourceRuleApproved . "[" . $typeRuleSource . "]," . $DestinationRuleApproved . "[" . $typeRuleDest . "])";
                                break;
                            }


                            if ($DestinationRuleApproved == "ok") {
                                $RuleApproved = true;
                                $priceRule = $oRule->getprice();
                                $nameRule = $oRule->getnameRule();
                                $idRule = $oRule->getid();
                                $logParsingRule = $logParsingRule . "," . $oRule->getid() . "(" . $SourceRuleApproved . "[" . $typeRuleSource . "]," . $DestinationRuleApproved . "[" . $typeRuleDest . "])";
                                break;
                            }

                        }

                    }
                    $logParsingRule = $logParsingRule . "," . $oRule->getid() . "(" . $SourceRuleApproved . "[" . $typeRuleSource . "]," . $DestinationRuleApproved . "[" . $typeRuleDest . "])";
                }


            }

            $discount = 1.00;

            if ($RuleApproved == true) {
                $estimated_fare = $priceRule;
            } else {
                if ($distance > 0) $carFare = 17.00;
                if ($distance > 4) $carFare = 18.00;
                if ($distance > 8) $carFare = 19.00;
                if ($distance > 12) $carFare = 21.00;
                if ($distance > 16) $carFare = 22.00;
                if ($distance > 20) $carFare = 24.00;
                if ($distance > 24) $carFare = 25.00;
                if ($distance > 28) $carFare = 26.00;
                if ($distance > 32) $carFare = 26.50;
                if ($distance > 36) $carFare = 26.75;
                if ($distance > 40) $carFare = 27.00;
                if ($distance > 60) $error = "Your trip distance is greater than 60 km.";

//                if ($child_seats > 2) {
//                    $car_seats += ($child_seats - 2);
//                    $child_seats = 2;
//                }

                if ($child_seats == 1 && $car_seats == 2) $discount = 0.95;
                if ($child_seats == 2 && $car_seats == 2) $discount = 0.60;
                if (($child_seats + $car_seats) > 5) $discount = 0.80;

                $estimated_fare_basic = ($car_seats * $carFare) + ($child_seats * $carFare);
                //$estimated_fare_basic = $carFare + ($distance * $km_fare) + ($duration * $minutefare) + ($car_seats * $seatfare) + ($child_seats + $seatfare) + ($nbToll * $tollfare);
                $estimated_fare = $estimated_fare_basic * $discount;

                if (get_option('stern_taxi_fare_minimum') > 0) {
                    if ($estimated_fare_basic < get_option('stern_taxi_fare_minimum')) {
                        $estimated_fare = get_option('stern_taxi_fare_minimum');
                    } else {
                        $estimated_fare = $estimated_fare_basic;
                    }
                }
            }
            if ($stern_taxi_fare_round_trip == "true") {
                $estimated_fare = $estimated_fare * 2;
            }


            $estimated_fare = round($estimated_fare);
        }
        /*
        for ($i=0;$i<15;$i++) {
            $varalan[] = getGoogleMapsDataAlan($source,$destination);
        }*/

        if ($GoogleMapsDataStatus == "errorGoogleEmpty" or $DestinationRuleApproved == "errorGoogleEmpty" or $SourceRuleApproved == "errorGoogleEmpty") {
            $statusGoogleGlobal = "errorGoogleEmpty";
        } else {
            $statusGoogleGlobal = "ok";
        }

        $response["distance"] = $distance;
        $response["distanceHtml"] = $distanceHtml;
        $response["duration"] = $duration;
        $response["durationHtml"] = $durationHtml;
        $response["nbToll"] = $nbToll;
        $response["estimated_fare"] = $estimated_fare;
        $response["cartypes"] = $cartypes;
        $response["carFare"] = $carFare;
        $response["suitcases"] = $suitcases;
        $response["car_seats"] = $car_seats;
        $response["child_seats"] = $child_seats;
        $response["infant_seats"] = $infant_seats;
        $response["km_fare"] = $km_fare;

        if (isset($error)) $response["error"] = $error;
        $response["discount"] = $discount;


        //	$response["selectedCarTypeId"] = $selectedCarTypeId;
        //	$response["typeCalculation"] = $typeCalculation;
        $response["SourceRuleApproved"] = $SourceRuleApproved;
        $response["DestinationRuleApproved"] = $DestinationRuleApproved;
        $response["RuleApproved"] = $RuleApproved;
        $response["nameRule"] = $nameRule;
        $response["selectedCarTypeId"] = $selectedCarTypeId;
        $response["typeIdCarInRule"] = $typeIdCarInRule;
        //	$response["nbMaxRuleBeforeGoogleStop"] = $nbMaxRuleBeforeGoogleStop;
        $response["idRule"] = $idRule;
        //$response["oRule"] = (string)$oRule;
        //$response["GoogleMapsData"] = $GoogleMapsData;
        $response["statusGoogleGlobal"] = $statusGoogleGlobal;

        $response["dateTimePickUp"] = $dateTimePickUp;
        $response["dateTimePickUpstrtotime"] = strtotime($dateTimePickUp);
        //$response["destination"] = $destination;


        $response["logParsingRule"] = $logParsingRule;

        if (!isset($error)) {
            global $woocommerce;

            $woocommerce->session->set_customer_session_cookie(true);


            if (get_option('stern_taxi_fare_emptyCart') == 'true') {
                $woocommerce->cart->empty_cart();
            }

            $woocommerce->cart->add_to_cart(get_option('stern_taxi_fare_product_id_wc'));


            WC()->session->set('distance', $distance);
            WC()->session->set('distanceHtml', $distanceHtml);
            WC()->session->set('duration', $duration);
            WC()->session->set('durationHtml', $durationHtml);
            WC()->session->set('estimated_fare', $estimated_fare);
            WC()->session->set('selectedCarTypeId', $selectedCarTypeId);
            WC()->session->set('cartypes', $cartypes);
            WC()->session->set('source', $source);
            WC()->session->set('destination', $destination);
            WC()->session->set('car_seats', $car_seats);
            WC()->session->set('child_seats', $child_seats);
            WC()->session->set('infant_seats', $infant_seats);
            WC()->session->set('dateTimePickUp', $dateTimePickUp);
            WC()->session->set('nbToll', $nbToll);
            WC()->session->set('stern_taxi_fare_round_trip', $stern_taxi_fare_round_trip);
            WC()->session->set('dateTimePickUpRoundTrip', $dateTimePickUpRoundTrip);


            //update_option("stern_taxi_fare_TEST_ALAN",'stern_taxi_fare_TEST_ALAN');
        }

        echo json_encode($response);
        wp_die();
    }

}

